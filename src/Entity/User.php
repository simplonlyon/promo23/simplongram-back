<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: UserRepository::class)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $profilePicture = null;

    #[Ignore]
    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: Picture::class)]
    private Collection $pictures;

    #[Ignore]
    #[ORM\ManyToMany(targetEntity: Picture::class, mappedBy: 'likes')]
    private Collection $likedPictures;

    public function __construct()
    {
        $this->pictures = new ArrayCollection();
        $this->likedPictures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getProfilePicture(): ?string
    {
        return $this->profilePicture;
    }

    public function setProfilePicture(?string $profilePicture): self
    {
        $this->profilePicture = $profilePicture;

        return $this;
    }

    /**
     * @return Collection<int, Picture>
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures->add($picture);
            $picture->setOwner($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->pictures->removeElement($picture)) {
            // set the owning side to null (unless already changed)
            if ($picture->getOwner() === $this) {
                $picture->setOwner(null);
            }
        }

        return $this;
    }

    public function getAvatar() {
        if(str_starts_with($this->profilePicture, 'http')) {
            return $this->profilePicture;
        } 
        

        return $_ENV['SERVER_URL'].'uploads/'.$this->profilePicture;
    }
    public function getAvatarThumbnail() {
        if(str_starts_with($this->profilePicture, 'http')) {
            return $this->profilePicture;
        } 
        
        return $_ENV['SERVER_URL'].'uploads/thumbnails/'.$this->profilePicture;
    }

    /**
     * @return Collection<int, Picture>
     */
    public function getLikedPictures(): Collection
    {
        return $this->likedPictures;
    }

    public function addLikedPicture(Picture $likedPicture): self
    {
        if (!$this->likedPictures->contains($likedPicture)) {
            $this->likedPictures->add($likedPicture);
            $likedPicture->addLike($this);
        }

        return $this;
    }

    public function removeLikedPicture(Picture $likedPicture): self
    {
        if ($this->likedPictures->removeElement($likedPicture)) {
            $likedPicture->removeLike($this);
        }

        return $this;
    }
}
