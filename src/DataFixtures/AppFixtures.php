<?php

namespace App\DataFixtures;

use App\Entity\Picture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i=0; $i < 10; $i++) { 
            $picture = new Picture();
            $picture->setDescription($faker->paragraphs(3, true));
            $picture->setPublicationDate($faker->dateTimeThisMonth());
            $picture->setSrc('https://picsum.photos/seed/480/640');
            $manager->persist($picture);
        }

        $manager->flush();
    }
}
