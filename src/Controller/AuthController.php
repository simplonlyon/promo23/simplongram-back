<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\Uploader;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class AuthController extends AbstractController
{
    #[Route('/api/user', methods: 'POST')]
    public function register(Request $request, SerializerInterface $serializer, UserRepository $repo, UserPasswordHasherInterface $hasher, Uploader $uploader, JWTTokenManagerInterface $jwtManager): JsonResponse
    {
        $user = $serializer->deserialize($request->getContent(), User::class, 'json' );
        if($repo->findOneBy(['email' => $user->getEmail()]) != null) {
            return $this->json(['error' => 'User Already exists'], Response::HTTP_BAD_REQUEST);
        }
        if(!empty($user->getProfilePicture())) {
            $user->setProfilePicture($uploader->upload($user->getProfilePicture()));
        }

        $user->setPassword($hasher->hashPassword($user, $user->getPassword()));
        $user->setRoles(['ROLE_USER']);
        $repo->save($user, true);

        
        return $this->json(['user' => $user, 'token' => $jwtManager->create($user)]);
    }
}
