# Simplongram Backend
Backend pour une application d'upload et de consultation d'images

Aspects techniques :
* Tests fonctionnels
* Upload de fichier


## Upload

Dans le cas d'une API Rest, le flow d'upload peut être fait comme tel :
1. On reçoit dans une requête l'image encodé en base64 (très longue chaîne de caractère représentant les données de l'image)
2. On transforme le base64 en une image
3. On génére un nom de fichier unique (comme ça si des users uploads des images avec le même nom, il n'y aura pas de conflit)
4. On sauvegarde l'image avec son nouveau nom dans un dossier rendu accessible par le serveur (dans le cas de symfony c'est le dossier public par défaut, mais on pourrait créer aussi un autre dossier et le rendre disponible via un serveur http)
5. (Optionnel) On crée une miniature de l'image qu'on sauvegarde également
6. On stock le nom de l'image dans la base de données
7. On peut rajouter des getter dans l'entité qui vont renvoyé directement le lien absolu de l'image (avec le lien du serveur dedans), pour ça on peut créer une variable d'environnement SERVER_URL

Pour faire les étapes 2 à 5, on peut créer une petite classe service dont ça sera l'objectif :
```php
class Uploader {
    
    public function __construct(private ParameterBagInterface $params) {}

    public function upload(string $file):string {
        //On stock le chemin vers le dossier où on sauvegardera nos fichier    
        $pathToUploads = $this->params->get('kernel.project_dir') . '/public/uploads/';
        //Si le dossier n'existe pas, on le crée
        if(!is_dir($pathToUploads.'thumbnails')) {
            mkdir($pathToUploads.'thumbnails', 0777, true);
        }
        
        //On génère un nom de fichier unique (il serait peut être préférable de récupérer l'extension d'une manière ou d'une autre)
        $filename = uniqid() . '.jpg';
        $manager = new ImageManager();
        $img = $manager->make($file);

        //On sauvegarde l'image originale
        $img->save($pathToUploads . $filename);
        //On sauvegarde la miniature
        $img->crop(200,140)->save($pathToUploads.'thumbnails/'.$filename);

        return $filename;
    }
}
```


## Les Tests

### How To run
1. On rajoute une DATABASE_URL dans un fichier `.env.test`
2. `bin/console do:da:cr --env=test`
3. `bin/console do:mi:mi --env=test`
4. `bin/console do:fi:lo --env=test`
5. `bin/phpunit` (ou bien dans l'onglet test de vscode)


Avec Symfony, en l'absence de règles métiers externalisées du contrôleur, faire des tests unitaires (de chaque composant isolé du reste) revient souvent à configurer longtemps puis tester ce qu'on vient de configurer, donc pas forcément pertinent.

A la place, on opte ici pour des tests fonctionnels où on test directement nos routes comme on le ferait avec Thunder Client, mais programmatiquement (afin de pouvoir relancer tous les tests rapidement et facilement avant chaque push par exemple)

Exemple de test : 
```php
public function testGetById() {
    
    $client = static::createClient();
    //On fait une requête vers la route à tester
    $client->request('GET', '/api/picture/1');
    //On indique que dans ce cas on est sensé avoir un status http 200
    $this->assertResponseIsSuccessful();
    //On récupère le body de la réponse en json (et on le convertit en tableau)
    $body = json_decode($client->getResponse()->getContent(), true);
    //On vérifie que les données reçues ont la structures à laquelle on s'attend
    $this->assertIsInt($body['id']);
    $this->assertIsString($body['src']);
    $this->assertIsString($body['description']);
    $this->assertIsString($body['publicationDate']);
    new \DateTime($body['publicationDate']);
    
}

```

Pour assurer une répétabilité des tests, on s'assure de remettre la base de donnée de test à zéro avant chaque test en utilisant la library `dama/doctrine-test-bundle`

Pour l'installer on en fait un composer req et on rajoute dans le fichier `phpunit.xml.dist` :
```xml
    <extensions>
        <extension class="DAMA\DoctrineTestBundle\PHPUnit\PHPUnitExtension"/>
    </extensions>
```