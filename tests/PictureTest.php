<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PictureTest extends WebTestCase
{
    
    public function testSomething(): void
    {
        $client = static::createClient();
        //On dit au test de faire une requête en GET sur la route /example
        $client->request('GET', '/example');

        //On dit au test de vérifier qu'on a bien une réponse 200
        $this->assertResponseIsSuccessful();
        
        //On récupère le body en json renvoyé par la réponse http
        $body = json_decode($client->getResponse()->getContent(), true);

        //On dit au test de vérifier que dans la réponse on a bien telle ou telle valeur
        $this->assertIsString($body['message']);
        
    }


    public function testGetAll() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/picture');

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

        $this->assertCount(10, $body);
        $item = $body[0];
        $this->assertIsInt($item['id']);
        $this->assertIsString($item['src']);
        $this->assertIsString($item['description']);
        $this->assertIsString($item['publicationDate']);
        new \DateTime($item['publicationDate']);
        
    }
    
    public function testGetById() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/picture/1');

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

        $this->assertIsInt($body['id']);
        $this->assertIsString($body['src']);
        $this->assertIsString($body['description']);
        $this->assertIsString($body['publicationDate']);
        new \DateTime($body['publicationDate']);
        
    }
    /**
     * Il peut être intéressant de faire des test pour les scénarios où c'est prévu que ça
     * ne marche pas pour vérifier que ça ne marche pas comme on le souhaite (genre qu'on ait pas une 
     * erreur 500 juste pasqu'on récupère un élément qui existe pas)
     */
    public function testGetByIdNotFound() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/picture/100');

        $this->assertResponseStatusCodeSame(404);
    }

    public function testAddPicture() {
        
        $client = static::createClient();

        $json = json_encode([
            'description' => 'test description',
            'src' => '/test.jpg'
        ]);
        
        $client->request('POST', '/api/picture', content: $json);

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

        $this->assertIsInt($body['id']);
        $this->assertIsString($body['publicationDate']);
        new \DateTime($body['publicationDate']);

    }
    public function testAddPictureFailedValidation() {
        
        $client = static::createClient();

        $json = json_encode([
            'description' => 'test description'
        ]);
        
        $client->request('POST', '/api/picture', content: $json);

        $this->assertResponseStatusCodeSame(400);


    }
    public function testUpdatePicture() {
        
        $client = static::createClient();

        $json = json_encode([
            'description' => 'test description',
            'src' => '/test.jpg'
        ]);
        
        $client->request('PATCH', '/api/picture/1', content: $json);

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

        
        $this->assertEquals($body['id'], 1);
        $this->assertEquals($body['description'], 'test description');
        $this->assertEquals($body['src'], '/test.jpg');

    }
    public function testUpdatePictureFailedValidation() {
        
        $client = static::createClient();

        $json = json_encode([
            'src' => ''
        ]);
        
        $client->request('PATCH', '/api/picture/1', content: $json);

        $this->assertResponseStatusCodeSame(400);


    }
     
    public function testDelete() {
        
        $client = static::createClient();
        
        $client->request('DELETE', '/api/picture/1');

        $this->assertResponseIsSuccessful();
        
    }
    
    public function testDeleteNotFound() {
        
        $client = static::createClient();
        
        $client->request('DELETE', '/api/picture/100');

        $this->assertResponseStatusCodeSame(404);
    }
}
